import './App.css';
import React , {useState} from 'react';
import Expenses from './components/Expenses/Expenses';
import NewExpense from './components/NewExpense/NewExpense';
const App = () => {
  
  const DUMMY_EXPENSES=[
    {id:'e1',title:"Toilet Paper",amount:100,date:new Date(2021,2,12)},
    {id:'e2',title:"New Tv",amount:799,date:new Date(2021,3,23)},
    {id:'e3',title:"Car Intense",amount:297,date:new Date(2021,4,18)},
    {id:'e4',title:"New Desk (Wooden)",amount:450,date:new Date(2021,7,12)}
  ]
  const  [expenses,SetExpenses]=useState(DUMMY_EXPENSES);

  const addExpenseHandler = (expense) => {
    SetExpenses((prevExpenses) =>{
      return [expense, ...prevExpenses]
    })
  }
  return (
    <div className="App">
      <NewExpense onAddExpense={addExpenseHandler}/>
      <Expenses  items={expenses}/>
    </div>
  );

// return React.createElement('div',{}, 
 //React.createElement('h2', {},'lets get Started'), 
 //React.createElement(Expenses,{items:expenses}))
}

export default App;
