import React from "react";
import ExpenseFrom from "./ExpenseForm";
import './NewExpense.css';
const NewExpense = (props) => {
    const  saveExpenseDataHandler = (enterExpenseData) => {
        const expenseData = {
            ...enterExpenseData,
            id:Math.random().toString()
        };
        console.log(expenseData);
        props.onAddExpense(expenseData);
    }
    return(
        <div className="new-expense">
                <ExpenseFrom onSaveExpenseData={saveExpenseDataHandler} />
        </div>
    )
}

export default NewExpense;