import React ,  {useState} from "react";
import './ExpenseForm.css';

const ExpenseFrom = (props) =>{
    //approche 1 state
    const [enteredTitle,SetEnteredTitle]= useState('');
    const [enteredAmount,SetEnteredAmount]= useState('');
    const [enteredDate,SetEnteredDate]= useState('');

    //approche 2 State
  /*  const  [userInput,setUserInput]=useState({
        enteredTitle:'',
        enteredAmount:'',
        enteredDate:'',
    })
 */
    const titleChangeHandler = (event) =>{
        //approche 1 state
        SetEnteredTitle(event.target.value);

          //approche 2 state
      /*   setUserInput({
            ...userInput,
            enteredTitle:event.target.value
        }) */

        //approche 2++ state
       /*  setUserInput((prevState) => {
           return {...prevState,enteredTitle:event.target.value}
        }) */
    }

    const amountChangeHandler = (event) =>{
         //approche 1 state
         SetEnteredAmount(event.target.value);

         //approche 2 state
       /*  setUserInput({
            ...userInput,
            enteredAmount:event.target.value
        }) */

        //approche 2++ state
        /* setUserInput((prevState) =>{
            return { ...prevState,enteredAmount:event.target.value}
        }) */
    }

    const dateChangeHandler = (event) =>{
        //approche 1 state
        SetEnteredDate(event.target.value);

         //approche 2 state
       /*  setUserInput({
            ...userInput,
            enteredDate:event.target.value
        }) */

        
         //approche 2++ state
        /* setUserInput((prevState) => {
            return { ...prevState,enteredDate:event.target.value}
        }) */
    }

    const SubmitHandler = (event) =>{
        event.preventDefault();
        const expenseData={
            title:enteredTitle,
            amount:enteredAmount,
            date:new Date(enteredDate)
        }
        props.onSaveExpenseData(expenseData);
        console.log(expenseData);
        SetEnteredAmount('');
        SetEnteredDate('');
        SetEnteredTitle('');
    }
    return (
    <form onSubmit={SubmitHandler}>
        <div className="new-expense__controls">
            <div className="new-expense__control">
                <label>Title</label>
                <input type='text' value={enteredTitle} onChange={titleChangeHandler}/>
            </div>
            <div className="new-expense__control">
                <label>Amount</label>
                <input type='number' min='0.01' step='0.01' value={enteredAmount} onChange={amountChangeHandler} />
            </div>
            <div className="new-expense__control">
                <label>Date</label>
                <input type='date' min='2019-01-01' max='2022-12-31'value={enteredDate} onChange={dateChangeHandler} />
            </div>
        </div>
        <div className="new-expense__actions">
            <button type="submit">Add Expense</button>

        </div>
    </form>
    )
};

export default ExpenseFrom;