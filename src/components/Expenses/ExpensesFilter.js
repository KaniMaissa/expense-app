import React from 'react';

import './ExpensesFilter.css';

const ExpensesFilter = (props) => {
  const dropdownChangeHandler = (event) => {
    props.onChangeFilter(event.target.value);
  };

  return (
    <div className='expenses-filter'>
      <div className='expenses-filter__control'>
        <label>Filter by year</label>
        <select value={props.selected} onChange={dropdownChangeHandler}>
          <option value='200'>200</option>
          <option value='100'>100</option>
          <option value='150'>150</option>
          <option value='800'>800</option>
        </select>
      </div>
    </div>
  );
};

export default ExpensesFilter;